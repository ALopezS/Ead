<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'respuesta';

    /**
     * Run the migrations.
     * @table respuesta
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('IDRespuesta');
            $table->string('DescripcionRespuesta', 100);
            $table->integer('administrador_IDAdmin')->nullable();
            $table->integer('jefedep_IDJefeD')->nullable();

            $table->index(["jefedep_IDJefeD"], 'fk_respuesta_jefedep1_idx');

            $table->index(["administrador_IDAdmin"], 'fk_respuesta_administrador_idx');


            $table->foreign('administrador_IDAdmin', 'fk_respuesta_administrador_idx')
                ->references('IDAdmin')->on('administrador')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('jefedep_IDJefeD', 'fk_respuesta_jefedep1_idx')
                ->references('IDJefeD')->on('jefedep')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
