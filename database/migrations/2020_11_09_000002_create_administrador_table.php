<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministradorTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'administrador';

    /**
     * Run the migrations.
     * @table administrador
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('IDAdmin');
            $table->string('ContrasenaAdmin', 30);
            $table->string('MatriculaAdmin', 20);
            $table->char('NombreAdmin', 20);
            $table->char('APaternoAdmin', 20);
            $table->char('AMaternoAdmin', 20);
            $table->string('CorreoAdmin', 40);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
